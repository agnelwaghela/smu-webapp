source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'devise'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', '4.0.5'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '2.1.2'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  
  gem 'rspec-rails', '3.2.3'
  gem 'spring-commands-rspec'
  gem 'guard-rspec', require: false
  gem 'fabrication'
end

group :test do
  gem 'minitest-reporters', '1.0.5'
  gem 'mini_backtrace', '0.1.3'
  gem 'guard-minitest', '2.3.1'
  gem 'capybara', '2.4.4'
  gem 'faker'
  gem 'shoulda-matchers', '2.8.0', require: false
end

group :production do
  gem 'pg'
  gem 'rails_12factor'
end

group :development do
  gem 'pry'
  gem 'pry-nav'
end

gem 'carrierwave', '0.10.0'
gem 'mini_magick', '3.8.0'
gem 'will_paginate', '3.0.7'
gem 'bootstrap-will_paginate', '0.0.10'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

gem 'fog', '1.36.0'

gem "wkhtmltopdf" # pdf binary used by wicked_pdf
gem "prawn" # pdf generation

gem "barby" # bar code generation

gem "bootstrap-sass" # bootstrap support
gem "better_errors"
gem "chunky_png" # for generating png images
gem "deadweight"
gem "font-awesome-sass" # font-awesome icons
#gem "iconv"
gem "json" # json support
#gem "launchy"
#gem "less-rails"
gem "logstasher" # better logging or errors
gem "roo" # CSV, Excel support
gem "writeexcel" # Excel support
gem "wicked_pdf" # pdf generation
gem "whenever" # schedule cron jobs https://github.com/javan/whenever